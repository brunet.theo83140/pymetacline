"""
This script search a list of id in a fasta and return their sequences
"""

import argparse
import re

def is_fasta(file_path):
    """confirm if the file is a fasta file or not"""
    try:
        file = open(file_path)
    except:
        raise argparse.ArgumentTypeError('not a file')
    for line in file:
        if line[0] != '>':
            if re.search(r'[^ACDEFGHIKLMNOPQRSTUVWY]',\
                    line.upper().replace('\n', '')) is not None:
                raise argparse.ArgumentTypeError('not a fasta file')
    file.close()


def create_parser():
    """create a parser -i for the input file and -seq for the list of id"""
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inputfile', type=is_fasta)
    parser.add_argument('-seq', type=str)
    return parser

def main():
    """Main fonction of the script"""
    parser = create_parser()
    args = parser.parse_args()
    args = args.__dict__
    print(args['inputfile'])
    print(args['seq'])
    fasta_file = open(args['inputfile'])
    list_seq = args['seq'].split(',')
    select = False
    for line in fasta_file:
        if select and line[0] != ">":
            print(line)
        if line[0] == '>':
            select = False
            if line[1:].replace('\n', '') in list_seq:
                print(line[:-1])
                select = True
    fasta_file.close()


if __name__ == "__main__":
    main()
